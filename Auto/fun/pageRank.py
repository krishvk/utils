#! /usr/bin/env python3
# ------------------------------------------------
# Author:    krishna
# USAGE:
#       pageRank.py
# Description:
#
#
# ------------------------------------------------
import numpy as np


def main():
    '''The Main'''

    pages = np.array ([
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0],
        [0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
        [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ])

    d = 0.85
    assert pages.shape[0] == pages.shape[1], "Not a square matrix"
    pRange = range(pages.shape[0])

    # print(prevRank)
    # inDegree = pages.sum(axis=0)

    outDegree = pages.sum(axis=1)
    print(outDegree)
    prevRank = [1 / pages.shape[0]] * pages.shape[0]

    for x in range(10):
        curRank = []
        for i in pRange:
            curRank.append(1-d+d*(sum(prevRank[j] / outDegree[j] for j in pRange if pages[j][i])))

        print(np.around(curRank, decimals=3))
        prevRank = curRank


if __name__ == '__main__':
    main()
