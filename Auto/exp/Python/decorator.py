#! /usr/bin/env python3
# ------------------------------------------------
# Author:    krishna
# USAGE:
#       decorator.py
# Description:
#
#
# ------------------------------------------------
def decWithArgs(**decArgs):
    def decorator(fun):
        def wrap(*args):
            print(f"{decArgs.get('x', 0)} Before fun")
            fun(*args)

        return wrap
    return decorator


@decWithArgs(x=1)
def fun():
    print("Inside fun")


@decWithArgs()
def funWithArgs(i):
    print(f"Inside fun {i}")


def main():
    '''The Main'''

    fun()
    funWithArgs(9)


if __name__ == '__main__':
    main()
