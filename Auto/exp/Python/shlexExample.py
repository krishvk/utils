#! /usr/bin/env python3
# ------------------------------------------------
# Author:    krishna
# USAGE:
#       blank.py
# Description:
#
#
# ------------------------------------------------
import shlex


def main():
    '''The Main'''

    print(shlex.split("a b c"))
    print(shlex.split("a\ b c"))
    print(shlex.split("a 'b c'"))

if __name__ == '__main__':
    main()
