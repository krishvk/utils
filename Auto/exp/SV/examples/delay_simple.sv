program test;

bit a, b, c, d, e, f;

initial $monitor($time, " ", a,b,c,d,e,f);

initial begin
a = #1 1'b1;
b = #2 1'b1;
c = #4 1'b1;
end

initial begin
d <= #1 1'b1;
e <= #2 1'b1;
f <= #4 1'b1;
end

endprogram : test