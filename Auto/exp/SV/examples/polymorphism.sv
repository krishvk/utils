program test;
  class first;
    virtual function void show();
      $write("1 ");
    endfunction
  endclass : first

  class second extends first;
    function void show();
      $write("2 ");
    endfunction
  endclass : second

  initial begin
    first f = new();
    second s = new();

    f.show();
    s.show();

    // f = s; // Statement - 1
    // s = f; // Statement - 2

    f.show();
    s.show();

    $display();
  end

endprogram : test
