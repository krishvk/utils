program test;

class c;

const int cnst;

function new(input int i);
    this.cnst = i;
endfunction : new

function show(input int i=cnst);
    $displayh(">>> ", i);
endfunction : show

endclass : c

initial begin
    c co = new(10);
    $displayh(">>> ", co.cnst);
    co.show();
end

endprogram : test