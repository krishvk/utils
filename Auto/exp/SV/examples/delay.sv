module test;

int a, b, c;

initial begin
    $monitor($time, a, b, c);
    #5 a = 5;
end

initial #10 b <= a;
initial c <= #10 a;

endmodule : test
