#! /usr/bin/env python3
# ------------------------------------------------
# Author:    krishna
# USAGE:
#       calc.py
# Description:
#
#
# ------------------------------------------------
import sys
import struct
from math import *
import scipy.stats as st


def r2ieee(r, p='sp'):
    '''
    Returns the IEEE 754 representation (in hex) of the given real number

    If the second argument is given and equals to

    'hp' -> converts into half-precession (16-bit) representation
    'dp' -> converts into double-precession (64-bit) representation

    In any other case (default) converts into single-precession (32-bit) representation
    '''

    if p == 'hp':
        return struct.pack('>e', r).hex()

    if p == 'dp':
        return struct.pack('>d', r).hex()

    return struct.pack('>f', r).hex()


def ieee2r(i, p='sp'):
    'Same as r2ieee but converts in reverse'

    if p == 'hp':
        return struct.unpack('>e', bytes.fromhex(f'{i:04x}'))[0]

    if p == 'dp':
        return struct.unpack('>d', bytes.fromhex(f'{i:016x}'))[0]

    return struct.unpack('>f', bytes.fromhex(f'{i:08x}'))[0]


def main():
    '''The Main'''

    if len(sys.argv) < 2:
        return

    # Evaluate the string and print result
    i = eval(sys.argv[1])
    print(f'{i}')

    # If the type is int, print all conversions
    if type(i) == int:
        i &= 0xFFFF_FFFF_FFFF_FFFF
        print(f'{i:_x}\n{i:_b}')


if __name__ == '__main__':
    main()
