#! /usr/bin/env python3
'''
Author:         krishna
Created:        Tue Jan 26 13:22:47 IST 2011
Usage:          New interpreter FileName
Description:    Creates a new script file with header and few obvious lines of code
'''
import getpass
import os
import sys
import shutil
import textwrap

from datetime import datetime, timezone


def getPythonFooter(scriptName):
    return textwrap.dedent(
        f"""\
        import argparse
        # import argparseapi


        def getOptParser():
            '''Returns the parser object for documentation purpose'''

            parser = argparse.ArgumentParser(
                allow_abbrev=True,
                formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                description='''Add description here'''
            )

            # parser.add_argument()
            return parser


        # @argparseapi.api(getOptParser())
        def {scriptName}(opts, **kwargs):
            '''The main function, that can also be called fromother scripts as an API'''

            return


        if __name__ == '__main__':
            {scriptName}(getOptParser().parse_args())

        """
    )


def main():
    '''Main function'''

    assert (len(sys.argv) == 3), "Improper arguments, expecting an interpreter and a FileName"

    interpreter, fileName = sys.argv[1:]
    scriptName  = os.path.splitext(os.path.basename(fileName))[0]

    assert shutil.which(interpreter), f"Could not find {interpreter}"
    assert not os.path.lexists(fileName), f"The path {fileName} already exists"

    headerRows = [
        f"Author:       {getpass.getuser()}",
        f"Created:      {datetime.now(timezone.utc).astimezone().strftime('%c %z')}",
        f"USAGE:        {os.path.basename(fileName)}",
        "Description:"
    ]

    if interpreter.startswith("python"):
        seperator, prefix = "'''", ''
    else:
         seperator, prefix = f'# {"-"*10}', '# '

    with open (fileName, "w", encoding='utf-8') as f:
        f.write(f"#! /usr/bin/env {interpreter}\n")
        f.write(seperator + "\n")
        for row in headerRows:
            f.write(f"{prefix}{row}\n")
        f.write(seperator + "\n")
        if interpreter.startswith("python"):
            f.write(getPythonFooter(scriptName))

    os.chmod(fileName, 0o700)


if __name__ == '__main__':
    main()
