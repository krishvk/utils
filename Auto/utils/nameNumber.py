import sys

def name_number(data):
    data = data.lower()

    # Strip non-alphabets
    data = ''.join(filter(str.isalpha, data))

    res = sum((ord(char) - ord('a') + 1) for char in data)
    while res >= 10:
        res = sum(int(digit) for digit in str(res))

    return res

if __name__ == "__main__":
    data = ''.join(sys.argv[1:])
    result = name_number(data)
    print(result)
