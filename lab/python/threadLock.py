#! /usr/bin/env python3
import contextlib
from threading import Thread, Lock
from time import sleep


class Counter:
    def __init__(self):
        self.value = 0
        self.lock = Lock()


    @contextlib.contextmanager
    def nonblocking(self, lock):
        locked = lock.acquire(blocking=False)
        try:
            yield locked
        finally:
            if locked:
                lock.release()


    def increaseNb(self, by):
        'Increment with non blocking lock'

        with self.nonblocking(self.lock) as locked:
            if locked:
                current_value = self.value
                current_value += by

                sleep(0.01)

                self.value = current_value
                print(f'counter incremented by {by} to {self.value}')


    def increase(self, by):
        'Increment with lock'

        with self.lock:
            current_value = self.value
            current_value += by

            sleep(0.01)

            self.value = current_value


    def increaseNl(self, by):
        'Increment without lock to demonstrate non threadsafe functions'

        current_value = self.value
        current_value += by

        sleep(0.01)

        self.value = current_value


def main():
    counter = Counter()

    for func in (counter.increaseNl, counter.increase, counter.increaseNb):
        counter.value = 0
        print(f'Using {func.__name__}')
        threads = [Thread(target=func, args=(i, )) for i in range(1, 101)]

        for t in threads:
            t.start()

        for t in threads:
            t.join()

        print(f'The final counter is {counter.value}')


if __name__ == '__main__':
    main()
