#! /usr/bin/env python3
# ------------------------------------------------
# Author:    kasula
# USAGE:
#       lsts.py
# Description:
#   Print the numbers that take longest number of steps go below initial number
#   Longest Steps To Small
# ------------------------------------------------
import sys

maxL = 0


def main():
    '''The Main'''

    # Take next odd number
    n = int(sys.argv[1])
    if n == 1:
        n = 3
    if not n & 1:
        n += 1

    while True:
        x, leaps, steps = n, 0, 0

        while True:
            x = x * 3 + 1
            while x & 1 == 0:
                x >>= 1
                steps += 1 # For divisions

            # Count the leaps
            leaps += 1
            steps += 1 # For 3n+1

            if x < n:
                break

        global maxL
        if leaps > maxL:
            maxL = leaps
            print(f'{n:16d} {n:<64b} {leaps:10d}')

        n += 2


if __name__ == '__main__':
    # for i in (
    #     1, 3, 7, 15, 27, 255, 447, 639, 703, 1819, 4255, 4591, 9663, 20895, 26623, 31911, 60975,
    #     77671, 113383, 138367, 159487, 270271, 665215, 704511, 1042431, 1212415, 1441407, 1875711,
    #     1988859, 2643183, 2684647, 3041127, 3873535, 4637979, 5656191
    # ):
    #     print(f'{i:10d} {i:64b}')
    main()
