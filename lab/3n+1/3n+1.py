#! /usr/bin/env python3
# ------------------------------------------------
# Author:    kasula
# USAGE:
#       3n+1.py
# Description:
#
#
# ------------------------------------------------
import sys
# import numpy as np
from tabulate import tabulate

data = dict()

def show(x, verbose=False, heighestOnly=False):
    n = x
    lastN = n
    fullN = n
    cnt = 0

    out = []
    out.append([n, fullN, int(bin(fullN)[2:]), int(bin(n)[2:])]) # len(bin(fullN))-2,
    # out[-1].append(cnt if cnt else '')
    while n > 1:
        lastN = n
        n = 3 * n + 1
        fullN = 3 * fullN + (fullN & ~(fullN - 1))
        while n & 1 == 0:
            n >>= 1

        if lastN < n:
            cnt += 1
        else:
            cnt = 0
        out.append([n, fullN, int(bin(fullN)[2:]), int(bin(n)[2:])]) # len(bin(fullN))-2,

    if verbose:
        print(tabulate(out, tablefmt="plain"))
    if heighestOnly:
        print(x, max(i[0] for i in out))
    l = len(bin(x)) - 2
    if l not in data or data[l]['steps'] < len(out):
        data[l] = {'l': l, 'num': x, 'r': x/((1<<l)-1), 'numb': int(bin(x)[2:]), 'steps': len(out)}


def showSummary(n):
    # Get to first odd number
    while n & 1 == 0:
        n >>= 1

    while n > 1:
        show(n)
        n -= 2

    print(tabulate(data.values(), headers='keys'))


def main():
    '''The Main'''

    n = int(eval(sys.argv[1]))
    # showSummary(n)
    show(n, verbose=True)
    return
    for n in range(1, 1<<15, 2):
        show(n, heighestOnly=True)



if __name__ == '__main__':
    main()
