#! /usr/bin/env python3
# ------------------------------------------------
# Author:    kasula
# USAGE:
#       plotOdd.py
# Description:
# 
# 
# ------------------------------------------------
import sys
import networkx as nx
import matplotlib.pyplot as plt


def main():
    '''The Main'''

    # g = nx.DiGraph(strict=True, directed=True, overlap=False, nodesep=0.1)
    seen = dict()

    # Get the starting odd number
    n = int(sys.argv[1])

    if n == 1:
        n = 3

    if not n & 1:
        n += 1

    while n < 1000:
        x = n

        while x >= n:
            xNxt = 3 * x
            while xNxt & 1:
                xNxt >>= 1
            xNxt += 1

            # g.add_edge(f'{x:d}:{x:b}', f'{xNxt:d}:{xNxt:b}')
            print(f'{x:d}:{x:b}', f'{xNxt:d}:{xNxt:b}')

            x = xNxt

            if x in seen:
                break
            else:
                seen[x] = 1

        n += 2

    # nx.draw(g, pos=nx.spring_layout(g), node_color='w')
    # plt.savefig('odd.svg')


if __name__ == '__main__':
    main()

