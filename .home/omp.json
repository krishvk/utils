{
    "$schema": "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json",
    "blocks": [
        {
            "type": "prompt",
            "alignment": "left",
            "segments": [
                {
                    "background": "#93a34a",
                    "foreground": "#111111",
                    "style": "powerline",
                    "powerline_symbol": "",
                    "template": " {{ .UserName }} ",
                    "type": "session"
                },
                {
                    "background": "#7847ff",
                    "foreground": "#ffffff",
                    "style": "powerline",
                    "powerline_symbol": "",
                    "properties": {
                        "folder_separator_icon": "  ",
                        "home_icon": "~",
                        "style": "folder"
                    },
                    "template": "   {{ .Path }} ",
                    "type": "path"
                },
                {
                    "background": "#38ff53",
                    "background_templates": [
                        "{{ if or (.Working.Changed) (.Staging.Changed) }}#FF9248{{ end }}",
                        "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#ff4500{{ end }}",
                        "{{ if gt .Ahead 0 }}#B388FF{{ end }}",
                        "{{ if gt .Behind 0 }}#B388FF{{ end }}"
                    ],
                    "foreground": "#193549",
                    "style": "powerline",
                    "powerline_symbol": "",
                    "properties": {
                        "branch_max_length": 25,
                        "fetch_stash_count": true,
                        "fetch_status": true,
                        "fetch_upstream_icon": true
                    },
                    "template": " {{ .UpstreamIcon }}{{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ",
                    "type": "git"
                },
                {
                    "background": "#6CA35E",
                    "foreground": "#ffffff",
                    "properties": {
                        "fetch_version": true
                    },
                    "style": "powerline",
                    "powerline_symbol": "",
                    "template": "  {{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }}{{ .Full }} ",
                    "type": "node"
                },
                {
                    "background": "#8ED1F7",
                    "foreground": "#111111",
                    "powerline_symbol": "",
                    "properties": {
                        "fetch_version": true
                    },
                    "style": "powerline",
                    "template": "  {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ",
                    "type": "go"
                },
                {
                    "background": "#4063D8",
                    "foreground": "#111111",
                    "powerline_symbol": "",
                    "properties": {
                        "fetch_version": true
                    },
                    "style": "powerline",
                    "template": "  {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ",
                    "type": "julia"
                },
                {
                    "background": "#AE1401",
                    "foreground": "#ffffff",
                    "powerline_symbol": "",
                    "properties": {
                        "display_mode": "files",
                        "fetch_version": true
                    },
                    "style": "powerline",
                    "template": "  {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ",
                    "type": "ruby"
                },
                {
                    "background": "#FEAC19",
                    "foreground": "#ffffff",
                    "powerline_symbol": "",
                    "properties": {
                        "display_mode": "files",
                        "fetch_version": false
                    },
                    "style": "powerline",
                    "template": " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ",
                    "type": "azfunc"
                },
                {
                    "background_templates": [
                        "{{if contains \"default\" .Profile}}#FFA400{{end}}",
                        "{{if contains \"jan\" .Profile}}#f1184c{{end}}"
                    ],
                    "foreground": "#ffffff",
                    "powerline_symbol": "",
                    "properties": {
                        "display_default": false
                    },
                    "style": "powerline",
                    "template": "  {{ .Profile }}{{ if .Region }}@{{ .Region }}{{ end }} ",
                    "type": "aws"
                },
                {
                    "background": "#ffff66",
                    "foreground": "#111111",
                    "powerline_symbol": "",
                    "style": "powerline",
                    "template": "  ",
                    "type": "root"
                },
                {
                    "background": "#00897b",
                    "background_templates": [
                        "{{ if gt .Code 0 }}#e91e63{{ end }}"
                    ],
                    "foreground": "#ffffff",
                    "properties": {
                        "always_enabled": true
                    },
                    "template": "  ",
                    "powerline_symbol": "",
                    "style": "powerline",
                    "type": "status"
                }
            ]
        },
        {
            "type": "rprompt",
            "alignment": "left",
            "segments": [
                {
                    "background": "#83769c",
                    "foreground": "#ffffff",
                    "properties": {
                        "always_enabled": true
                    },
                    "powerline_symbol": "",
                    "style": "powerline",
                    "template": "  {{ .FormattedMs }} ",
                    "type": "executiontime"
                },
                {
                    "background": "#FFDE57",
                    "foreground": "#111111",
                    "powerline_symbol": "",
                    "properties": {
                        "display_mode": "environment",
                        "fetch_virtual_env": true
                    },
                    "style": "powerline",
                    "template": "  {{ if .Venv }}({{ .Venv }}){{ end }} ",
                    "type": "python"
                },
                {
                    "background": "#0077c2",
                    "foreground": "#ffffff",
                    "powerline_symbol": "",
                    "style": "powerline",
                    "template": "  {{ .Name }} ",
                    "type": "shell"
                },
                {
                    "background": "#f36943",
                    "background_templates": [
                        "{{if eq \"Charging\" .State.String}}#40c4ff{{end}}",
                        "{{if eq \"Discharging\" .State.String}}#ff5722{{end}}",
                        "{{if eq \"Full\" .State.String}}#4caf50{{end}}"
                    ],
                    "foreground": "#ffffff",
                    "powerline_symbol": "",
                    "properties": {
                        "charged_icon": " ",
                        "charging_icon": " ",
                        "discharging_icon": " "
                    },
                    "style": "powerline",
                    "template": " {{ if not .Error }}{{ .Icon }}{{ .Percentage }}{{ end }}{{ .Error }} ",
                    "type": "battery"
                },
                {
                    "background": "#2e9599",
                    "foreground": "#111111",
                    "powerline_symbol": "",
                    "style": "powerline",
                    "template": " {{ .CurrentDate | date .Format }}",
                    "type": "time"
                }
            ]
        }
    ],
    "console_title_template": "{{ .Shell }} in {{ .Folder }}",
    "final_space": true,
    "shell_integration": true,
    "patch_pwsh_bleed": true,
    "upgrade": {
        "notice": true,
        "interval": "24h",
        "auto": false,
        "source": "cdn"
    },
    "version": 3
}
