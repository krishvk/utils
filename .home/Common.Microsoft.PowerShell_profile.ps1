function assoc { cmd /c assoc $args }
function ftype { cmd /c ftype $args }


$env:VIRTUAL_ENV_DISABLE_PROMPT = 1
oh-my-posh init pwsh --config "$Env:USERPROFILE\den\git\utils\.home\omp.json" | Invoke-Expression
Import-Module -Name Terminal-Icons, CompletionPredictor, PSFzf
Set-PSReadLineOption -PredictionViewStyle ListView -PredictionSource HistoryAndPlugin
Set-PsFzfOption -PSReadLineChordProvider ‘Ctrl+f’ -PSReadLineChordReverseHistory ‘Ctrl+r’

$Env:PATH += ";$Env:USERPROFILE\Den\git\utils\Auto\utils"
$Env:PATHEXT += ";.py"

# Set below environment variables if they are not set
if (-not $Env:SCAN_HOME) {
    $Env:SCAN_HOME = "$Env:USERPROFILE\Den\git\ScAn"
}
